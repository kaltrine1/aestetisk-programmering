// Declare and initialize the variables for the program's state
let stage = -1; // Variable to represent the current stage of the program, initialized to -1 (start screen)
let correct = 0; // Variable to store the users count of correct answers, initialized to 0

// Declare and initialize boolean variables for the program state
let showErrorEnglish = false; // Flag to track if the English error screen should be shown
let showErrorBack = false; // Flag to track if the back button error screen should be shown
let youHavePassed = false; // Flag to track if the user has passed the program
let youHaveFailed = false; // Flag to track if the user has failed the program

let currentSelection = ''; // Tracks the currently selected answer

let approvedOrDeniedTitle = ''; // Variable to store different titles based on the outcome of the program
let passedOrFailedText = ''; // Variable to store different texts based on the outcome of the program
let englishOrBackErrorText = ''; // Variable to store different texts based on which error has occured

// Variable to store JSON file in
let test;

// Variables to store images in
let passedBackground;
let failedBackground;
let rainOverlay;
let planeOverlay;
let stampSticker;

// Variables to store sound in
let passedSong;
let failedSong;

// Variables to store img element in
let daLanguage;
let enLanguage;

// Declare variables for buttons
let startButton;
let nextButton;
let backButton;
let tlButton;
let trButton;
let blButton;
let brButton;

// Preload JSON file, images and sounds
function preload() {
    test = loadJSON('test.json'); // Load questions and answers from our JSON file
    passedBackground = loadImage('passed.jpg');
    failedBackground = loadImage('failed.jpg');
    rainOverlay = loadImage('rain.gif');
    planeOverlay = loadImage('plane.gif');
    stampSticker = loadImage('stampSticker.gif');
    passedSong = loadSound('passed.mp3');
    failedSong = loadSound('failed.mp3');
}

// Setup the initial elements and formats
function setup() {
    createCanvas(windowWidth, windowHeight);
    rectMode(CENTER);
    textAlign(CENTER, CENTER);

    setupStartButton(); // Display the start button 
    setupLanguageButtons(); // Display the language buttons
}

function draw() {
    background(213, 206, 194);

    container(width / 2, height / 2, width * 0.7, height * 0.7); // Function that draws a rectangle based on the arguments (position and size)

    if (stage == -1) { // If stage is -1
        startScreenText(); // Display start screen text
    }

    if (stage >= 0 && stage < test.questions.length) { // If stage is within question range
        showQuestion(); // Display the current question
    }

    if (showErrorEnglish) { // If showErrorEnglish is true
        errorEnglishScreen(); // Display English error screen
    }

    if (showErrorBack) { // If showErrorBack is true
        errorBackScreen(); // Display back error screen
    }

    if (stage == test.questions.length) { // If stage equals the total number of questions
        endOfTest(); // End the test
    }
}

// Function to create a rectangle with parameters for position and size
function container(x, y, w, h) {
    stroke(188, 176, 158);
    strokeWeight(3);
    fill(255);
    rect(x, y, w, h, 10);
}

// Function to create the start button for the start screen
function setupStartButton() {
    startButton = createButton('Start');
    startButton.position(width * 0.453, height * 0.65);
    startButton.size(width * 0.1, height * 0.08);
    startButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    startButton.mousePressed(startTest); // When start button is pressed, start the test
}

// Function to set stage to 0 to show the first screen of the test
function startTest() {
    stage = 0; // Set the stage to 0 (start the program)
    setupAndClearStage(); // Display the function that handles buttons
}

// Function to create text for the start screen
function startScreenText() {
    fill(0);
    noStroke();
    textSize(width * 0.025);
    text('Dansk Statborgerskabsprøve 2024', width / 2, height * 0.34);
    textSize(width * 0.012);
    text('For at blive dansk statsborger skal du gennemføre følgende prøve. Prøven er udstedet af Udlændinge- og Integrationsministeriet, som tager stilling til, om du er berettiget til dansk statsborgerskab efter prøvegennemførelsen. \n\n Klik på Start for at komme i gang.', width / 2, height / 2, width * 0.38);
}

// Function to handle questions from JSON file
function showQuestion() {
    let question = test.questions[stage]; // Variable to store the current stage's questions 
    if (stage < test.questions.length) { // If stage is less than total number of questions 
        textSize(35);
        noStroke();
        fill(0);
        text(question.text, width / 2, height * 0.34, width * 0.6); // Display the question text
    }
}

// Function to handle buttons 
function setupAndClearStage() {
    currentSelection = ''; // Reset selected answer
    if (stage >= 0 && stage < test.questions.length) { // If stage is within question range
        startButton.remove(); // Remove the start button
        clearButtons(); // Clear existing buttons
        setupNextButton(); // Display the next button
        setupBackButton(); // Display the back button
        setupLanguageButtons(); // Display the language settings
        setupAnswerButtons(); // Display the answer buttons
    }
}

// Function to create buttons
function setupAnswerButtons() {
    let question = test.questions[stage]; // Get the current question

    // Create and position the 4 answer buttons
    tlButton = createButton(question.answers[0]); // Display the current answer for the specific stage
    tlButton.position(width * 0.20, height / 2);
    tlButton.size(width * 0.28, height * 0.11);
    tlButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    tlButton.mousePressed(tlAnswer); // When top left button is pressed, select index 0 in the JSON file

    trButton = createButton(question.answers[1]); // Display the current answer for the specific stage
    trButton.position(width * 0.52, height / 2);
    trButton.size(width * 0.28, height * 0.11);
    trButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    trButton.mousePressed(trAnswer); // When top right button is pressed, select index 1 in the JSON file

    blButton = createButton(question.answers[2]); // Display the current answer for the specific stage
    blButton.position(width * 0.20, height / 1.51);
    blButton.size(width * 0.28, height * 0.11);
    blButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    blButton.mousePressed(blAnswer); // When bottom left button is pressed, select index 2 in the JSON file

    brButton = createButton(question.answers[3]); // Display the current answer for the specific stage
    brButton.position(width * 0.52, height / 1.51);
    brButton.size(width * 0.28, height * 0.11);
    brButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    brButton.mousePressed(brAnswer); // When bottom right button is pressed, select index 3 in the JSON file
}

// Named functions that creates a reusable function reference that mouse pressed can call when needed
// Passing arguments to our function
function tlAnswer() {
    selectedAnswer(0);
}

function trAnswer() {
    selectedAnswer(1)
}

function blAnswer() {
    selectedAnswer(2)
}

function brAnswer() {
    selectedAnswer(3)
}

// Function to create the language buttons
function setupLanguageButtons() {
    daLanguage = createImg('danishflag.png'); // Creates an img element
    daLanguage.size(width * 0.03, width * 0.03);
    daLanguage.position(width * 0.91, height * 0.02);
    daLanguage.addClass('danishFlagStyling'); // Add class to style the button from the style.css file

    enLanguage = createImg('englishflag.png'); // Creates an img element
    enLanguage.size(width * 0.03, width * 0.03);
    enLanguage.position(width * 0.95, height * 0.02);
    enLanguage.addClass('englishFlagStyling'); // Add class to style the button from the style.css file
    enLanguage.mousePressed(showErrorEnglishScreen); // When english button is pressed, whoe error screen
}

// Function to create the next button
function setupNextButton() {
    nextButton = createButton('Næste'); // Create a button element
    nextButton.position(width * 0.9, height * 0.9);
    nextButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    nextButton.mousePressed(nextQuestion); // When next button is pressed, show next question
    nextButton.attribute('disabled', 'true'); // Adds an attribute to the button element, the disabled attribute makes it unclickable 
}

// Function to create the back button
function setupBackButton() {
    backButton = createButton('Tilbage'); // Create a button element
    backButton.position(width * 0.04, height * 0.9);
    backButton.addClass('buttonStyling'); // Add class to style the button from the style.css file
    backButton.mousePressed(showErrorBackScreen); // When back button is pressed, show error screen
}

// Function to handle answer selection with the parameter index
function selectedAnswer(index) {
    currentSelection = index; // Assign the index to the current selection
    nextButton.removeAttribute('disabled'); // Removes the attribute on the button element, so you can click it again 
}

// Function to check if the current selected answer is the correct answer
function checkAnswer() {
    if (currentSelection == test.questions[stage].correctAnswer) { // if current selected answer is equal to the specific stage's test question answer 
        correct++; // Increase the amount of correct answers by 1
    }
}

// Function to move to the next question or end the test if all questions are answered
function nextQuestion() {
    if (stage >= 0) { // If stage is greater than or equal to 0
        checkAnswer(); // Check the answer 
    }
    stage++; // Move to the next stage
    if (stage < test.questions.length) { // If stage is less than amount of questions 
        setupAndClearStage(); // setup and clear buttons for next stage
    } else { // Else
        endOfTest(); // End the test 
        if (youHavePassed) { // If you have passed is true
            passedSong.play(); // Play the passed song
        }
        if (youHaveFailed) { // If you have failed is true 
            failedSong.play(); // Play the failed song
        }
    }
}

// Function to display the passed screen
function passedScreen() {
    youHavePassed = true; // Set passed status to true
    background(passedBackground, width, height);
    endOfTestStyling(); // Style the passed screen with the end of test styling
    approvedOrDeniedTitle = 'GODKENDT';
    passedOrFailedText = 'Kære ansøger, \n\n Tillykke! Du er nu dansk statsborger! \n\n Vi er glade for at meddele, at du nu er en ægte viking med et dansk statsborgerskab i hånden! Du har bestået statsborgertesten til UG, og du ville nu kunne blive en officiel del af det danske samfund. \n\n Velkommen til Danmark – vi er glade for at have dig med! \n\n Med venlig hilsen, \n Den Danske Adgangskomité';
}

// Function to display the failed screen
function failedScreen() {
    youHaveFailed = true; // Set failed status to true
    background(failedBackground, width, height);
    endOfTestStyling(); // Style the failed screen with the end of test styling
    approvedOrDeniedTitle = 'NÆGTET';
    passedOrFailedText = 'Kære ansøger, \n\n Øv, vi må desværre meddele at din indfødsretsansøgning er blevet afvist. \n\n Lad os tage et øjeblik til at reflektere over denne bedrift. For det kræver virkelig noget særligt at navigere gennem de utallige formularer, bizarre krav og mysterier om integrationsprøver, blot for at blive afvist ved målstregen -  det lyder meget trælst...\n\n Husk, du er ikke alene! Velkommen til klubben! \n\n Med venlig hilsen, \n Den Danske Afvisningskomité';
    image(planeOverlay, 0, 0, width, height);
    image(stampSticker, width * 0.515, height * 0.61, width * 0.22, height * 0.2);
    image(rainOverlay, 0, 0, width, height);
}

// Function with styling for the end of test screens
function endOfTestStyling() {
    clearButtons(); // Clear existing buttons
    container(width / 2, height / 2, width * 0.40, height * 0.60); // Function to create a rectangle with arguments 
    fill(0);
    noStroke();
    textSize(width * 0.022);
    textAlign(CENTER, CENTER);
    text('INDFØDSRET ' + approvedOrDeniedTitle, width / 2, height * 0.27);
    textSize(width * 0.018);
    text('Antal rigtige: ' + correct + '/' + test.questions.length, width / 2, height * 0.32);
    textSize(width * 0.012);
    textAlign(LEFT, CENTER);
    text(passedOrFailedText, width / 2, height * 0.56, width * 0.3, height * 0.7);
}

// Function to end the test and determine if the player has won or lost
function endOfTest() {
    if (correct == test.questions.length) { // If amount of correct answers is equal to the amount of questions
        passedScreen(); // Show the passed screen
    } else if (correct < test.questions.length) { // If amount of correct answers is less than the amount of questions
        failedScreen(); // Show the failed screen
    }
}

// Function to show the back button error screen
function showErrorBackScreen() {
    showErrorBack = true; // Set showErrorBack to true
    clearButtons(); // Clear existing buttons
}

// Function to show the English error screen
function showErrorEnglishScreen() {
    showErrorEnglish = true; // Set showErrorEnglish to true
    clearButtons(); // Clear existing buttons
}

// Function to create the back button error screen
function errorBackScreen() {
    background(33, 0, 177);
    fill(255);
    textSize(20);
    textAlign(LEFT, TOP);
    englishOrBackErrorText = "ERROR 406: Back Button Not Acceptable \n \n System Alert! \n [Code: 406] \n \n Det ser ud til, at du har forsøgt at gå tilbage. \n \n Fejlmeddelelse: \n BACK_NAV_ERROR: Back navigation is restricted. \n \n Beskrivelse: \n For at blive dansk statsborger skal du være sikker i din danskhed. At gå tilbage under indfødsretstesten er derfor ikke tilladt. \n Danmark værner om sin beslutsomhed og fremdrift, og som en kommende statsborger er det vigtigt, at du viser sikkerhed i dine valg. \n \n Mulige løsninger: \n 1. Bliv mere sikker i din danskhed. \n 2. Start forfra og fuldør uden usikkerhed. \n \n Technical Details: \n [Error Code: 406] \n [NavAction: BACK] \n [AllowedActions: FORWARD, SUBMIT] \n [Timestamp: 2024-06-03 12:34:56] \n \n Support: \n Har du spørgsmål eller brug for hjælp? Kontakt os på support@indfødsret.dk eller ring til 1234 5678. \n \n Husk, at beslutningsdygtighed er en vigtig del af det at være dansk. Prøv igen med selvsikkerhed og held og lykke med din test!";
    text(englishOrBackErrorText, 50, 50);
}

// Function to create the English error screen
function errorEnglishScreen() {
    background(33, 0, 177);
    fill(255);
    textSize(20);
    textAlign(LEFT, TOP);
    englishOrBackErrorText = "ERROR 406: Language Selection Not Acceptable \n \n System Alert! \n [Code: 406] \n \n Det ser ud til, at du har forsøgt at vælge engelsk som sprog til din indfødsretstest. \n \n Fejlmeddelelse: \n ENG_SEL_ERROR: Language selection invalid. \n \n Beskrivelse: \n For at blive dansk statsborger skal du kunne læse og forstå dansk. At vælge engelsk som sprog på en dansk indfødsretstest er derfor ikke tilladt. \n Danmark værner om sit sprog og kultur, og som en kommende statsborger er det vigtigt, at du er i stand til at kommunikere på dansk. \n \n Mulige løsninger: \n 1. Lær dansk. \n 2. Start forfra og fuldfør testen på dansk. \n \n Technical Details: \n [Error Code: 406] \n [LangSelection: ENG] \n [SupportedLang: DAN] \n [Timestamp: 2024-06-03 12:34:56] \n \n Support: \n Har du spørgsmål eller brug for hjælp? Kontakt os på support@indfødsret.dk eller ring til 1234 5678. \n \n Husk, at kunne dansk er en væsentlig del af det at være dansk. Prøv igen på dansk og held og lykke med din test!";
    text(englishOrBackErrorText, 50, 50);
}

// Function to clear existing buttons 
function clearButtons() {
    if (tlButton) { // If the tl button exists
        tlButton.remove(); // Remove it
    }
    if (trButton) { // If the tr button exists
        trButton.remove(); // Remove it
    }
    if (blButton) { // If the bl button exists
        blButton.remove(); // Remove it
    }  
    if (brButton) {  // If the br button exists
        brButton.remove(); // Remove it
    }
    if (backButton) { // If the back button exists
        backButton.remove(); // Remove it 
    }
    if (nextButton) { // If the next button exists
        nextButton.remove(); // Remove it 
    }
    if (startButton) { // If the start button exists
        startButton.remove(); // Remove it 
    }
    if (enLanguage) { // If the English language button exists
        enLanguage.remove(); // Remove it 
    }
    if (daLanguage) { // If the Danish language button exists
        daLanguage.remove(); // Remove it 
    }
}

// Function to resize the canvas when the window is resized
function windowResized() {
    resizeCanvas(windowWidth, windowHeight); 
}