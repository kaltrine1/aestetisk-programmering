function setup(){
    createCanvas(700,500);
    background('ivory');
    design();
    
    //Make a link for Movie123
    let FirstLink = createA('#','Movie123');
    FirstLink.position(125,235);
    FirstLink.style("font-size", "1.2em");
    FirstLink.mousePressed(showRectangle);


    //Make another link for Watch Free Movies
    let SecondLink = createA('#','Watch Free Movies');
    SecondLink.position(125,385);
    SecondLink.style("font-size", "1.2em");
    SecondLink.mousePressed(createVideoCapture);
}

function design(){
    //Google text
    fill(33,33,33);
    textFont('New Courier', 35);
    text('Google',20,50);

    //Search Bar
    fill('white');
    rect(140,25,530,30,15);

    //Seperating Line
    stroke(33,33,33);
    line(0,120,700,120);

    //Text 'All'
    fill(33,33,33);
    textFont('New Courier',15);
    text('All',150,110);

    //Text 'Shopping'
    fill(33,33,33);
    textFont('New Courier',15);
    text('Shopping',200,110);

    //Text 'Pictures'
    fill(33,33,33);
    textFont('New Courier',15);
    text('Pictures',285,110);

    //Text 'Videos'
    fill(33,33,33);
    textFont('New Courier',15);
    text('Videos',360,110);

    //Text 'News'
    fill(33,33,33);
    textFont('New Courier',15);
    text('News',430,110);

    //Text 'More'
    fill(33,33,33);
    textFont('New Courier',15);
    text('More',490,110);

    //Three small ellipse dots next to Text More
    fill(33,33,33);
    ellipse(485,109,1,1);
    fill(33,33,33);
    ellipse(485,105,1,1);
    fill(33,33,33);
    ellipse(485,101,1,1);

    //Thick line under Text 'All'
    push();
    stroke(33,33,33);
    strokeWeight(4);
    line(140,119,180,119);
    pop();

    //Magnifying Glass Symbol
    fill('none');
    circle(641,37,13,13);
    stroke(33,33,33);
    line(645,41,653,50);

    //Google Search 'Text'
    fill(33,33,33);
    textFont('New Courier',18);
    text('New Movie Releases',155,47);
    
    //Movie123 Google Search Logo
    fill('none');
    circle(135,200,30,30);

    //Logo in circle
    fill(183, 28, 28);
    textFont('New Courier',16);
    text('M',128,206);

    //Small title
    fill(33,33,33);
    textFont('New Courier',15);
    text('Movie123',160,195);

    //Website under the title
    fill(33,33,33);
    textFont('New Courier',12);
    text('https:/WatchMovie123.com',160,210);

    //Description of the website 
    fill(33,33,33);
    textFont('New Courier',13);
    text('Watch123 is a site provided for watching and streaming new movie releases, the streaming is completly free', 125,275);
    text('and safe. Press link to watch now!',125,295);

    //WatchFree Google Search Logo
    fill('none');
    circle(135,350,30,50);

    //Logo in circle
    fill(183, 28, 28);
    textFont('New Courier',15);
    text('MF',124,355);

    //Small title
    fill(33,33,33);
    textFont('New Courier',15);
    text('WatchFreeMovies',160,350);

    //Website under the title
    fill(33,33,33);
    textFont('New Courier',12);
    text('https:/WatchFreeMovies.com',160,365);

    //Description of the website 
    fill(33,33,33);
    textFont('New Courier',13);
    text('Watch Free Movies is a free website that has the newest movies and series released in 2024, the website', 125,425);
    text('is free and safe for watching for anyone. Only login is needed for free streaming!',125,445);
}

function showRectangle(){

    //Big box in the middle
    push();
    fill(191, 54, 12);
    rect(250,150,200,200,10);
    fill(255, 235, 59);
    textFont('New Courier',30);
    stroke(0);
    strokeWeight(4);
    text('!Virus',310,205);
    text('Detected!',295,240);
    fill(255, 235, 59);
    textFont('New Courier',20);
    stroke(0);
    strokeWeight(3);
    text('Personal Information',268,280);
    text('Is Being Leaked',290,310);
    pop();

    //Box in the top right corner
    push();
    fill(191, 54, 12);
    rect(510,60,150,100,20);
    fill(255, 235, 59);
    textFont('New Courier',25);
    stroke(0);
    strokeWeight(3);
    text('Installing',540,95);
    text('Malware',540,120);
    text('Software!',540,145);
    pop();

    //Box in the left bottom corner
    push();
    fill(191, 54, 12);
    rect(20,300,150,80,20);
    fill(255, 235, 59);
    textFont('New Courier',25);
    stroke(0);
    strokeWeight(3);
    text('Infected',55,330);
    text('Website!',55,360);
    pop();  
}


function createVideoCapture() {
    let videoCapture = createCapture(VIDEO);
    videoCapture.size(300, 300);
    videoCapture.position(200,130);

    //Box in the top right corner 
    push();
    fill(191, 54, 12);
    rect(530,170,150,70,20)
    fill(255, 235, 59);
    textFont('New Courier',20);
    stroke(0);
    strokeWeight(2);
    text('Camera',575,200);
    text('Turned On',565,225);
    pop();

    //Box on the bottom of VideoCapture
    push();
    fill(191, 54, 12);
    rect(200,410,302,70,20);
    fill(255, 235, 59);
    textFont('New Courier',20);
    stroke(0);
    strokeWeight(3);
    text('!Gained Full Camera Access!',230,450);
    pop();

    //Box in the left bottom corner
    push();
    fill(191, 54, 12);
    rect(20,300,150,80,20);
    fill(255, 235, 59);
    textFont('New Courier',25);
    stroke(0);
    strokeWeight(3);
    text('Infected',55,330);
    text('Website!',55,360);
    pop();  
    
}
