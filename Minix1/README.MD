**Press to See A Flower**
<br>

Description: A canvas where it is possible to press anywhere to see a flower appear. Depending on which half of the canvas you press, the flower will show in a specific color. 
<br>

Picture 1: 
![This is my picture](Flower1.png)
<br>
Picture 2: 
![This is my second picture](Flower2.png)

Here is a link to run the code: https://kaltrine1.gitlab.io/aestetiskprogrammering/Minix1/index.html?ref_type=heads
<br>

Here is a link for the whole miniX repository on Gitlab: https://gitlab.com/kaltrine1/aestetisk-programmering 
<br>

Reflections: What have you produced? <br>
The idea behind this RunMe was to make an interactive site, that allows the user to place a flower anywhere on the screen. The flower appears wherever the user clicks on the mouse. Depending on which half of the canvas that the user clicks on, the color of the flower changes. This is mainly because the function 'MousePressed' has been used. It allows the user to chose where on the canvas they want to see a flower, and it only appears where they have pressed. It is the 'if' statement that makes it possible to change the color of the flower, depending on where the mouse clicks. If the mouse is pressed on the left half of the canvas, a pink flower will appear, likewise the 'else if' statement makes it show a light blue flower on the right half. 
It is by taking the measurements of the canvas and dividing it by 2, and then assigning one flower to a certain half, that the color change is possible. 
It is possible to refresh the page to clear the canvas, and to start again. 
<br>

Reflections: How was it different from reading and writing text?: <br>
I found this coding experience quite interesting, mainly because we had creative freedom. It was a good experience for an introdcution into p5.js, and its different opportunities when it comes to aesthetic programming. My coding experience on this assignment was influenced by the lecture on changing the background, when the mouse is pressed on one half of the page, where I later implemented my solution to this into my own miniX1. It was furthermore the references on the p5.js website that helped me expand my knowledge on possible syntaxes, and how to use them. This experience was also different from reading and writing text, since I had to make myself familiar with new syntaxes, and because I had the chance to explore and 'play around' with the different possibilites that p5.js gives me. 
<br>

References: <br>
Text: https://p5js.org/reference/#/p5/text 
<br>
TextFont: https://p5js.org/reference/#/p5/textFont 
<br>
NoStroke: https://p5js.org/reference/#/p5/noStroke
<br>
Fill: https://p5js.org/reference/#/p5/fill
<br>
Circle: https://p5js.org/reference/#/p5/circle
<br>
MousePressed: https://p5js.org/reference/#/p5/mousePressed 
<br>
'If and Else if' statement: https://p5js.org/reference/#/p5/if-else and Lecture 8 (Aesthetic Programming, we had to give our own example on how to change background on half the screen)
<br>
Push: https://p5js.org/reference/#/p5/push
<br>
Pop: https://p5js.org/reference/#/p5/pop 
<br>
Translate: https://p5js.org/reference/#/p5/translate 
<br>
Markdown Template: From TA on BrightSpace: https://brightspace.au.dk/content/enforced/123560-LR28279/template.md 
<br>

