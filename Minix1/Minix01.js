function setup(){
  createCanvas(1430,810);
  background(255, 255, 240);
  textFont('Courier New',30);
  text('Press Anywhere To See A Flower', 470,300);
  textFont('Courier New',15);
  text('See the color difference on the left and right half', 515,340);
}

function mousePressed(){//The flower will appear where the user presses the mouse
  drawFlower(mouseX, mouseY);
}

function drawFlower(x,y){
  if(mouseX < 715){ //The left side of the canvas shows the flower in a specific color
  push(); //'Push' followed by 'Pop' at the bottom, makes sure that the flower and all its information is saved within these functions
  noStroke();
  translate(x,y); //This is to make sure that the flowers can be seen everywhere on the canvas, and it removes it from the default place
  fill(255,245,15);
  circle(5,50,60);
  fill(252,228,236);
  circle(36,105,50);
  fill(252,228,236);
  circle(65,50,50);
  fill(252,228,236);
  circle(40,0,50);
  fill(248,187,208);
  circle(-25,105,50);
  fill(248,187,208);
  circle(-20,-5,50);
  fill(248,187,208);
  circle(-55,50,50);
  pop();
} else if (mouseX > 715){ //The right side of the canvas shows the flower in another color, once the mouse is pressed there
  push(); //'Push' followed by 'Pop' at the bottom, makes sure that the flower and all its information is saved within these functions
  noStroke();
  translate(x,y); //This is to make sure that the flowers can be seen everywhere on the canvas, and it removes it from the default place
  fill(255,245,157);
  circle(5,50,60);
  fill(197,202,233);
  circle(36,105,50);
  fill(197,202,233);
  circle(65,50,50);
  fill(197,202,233);
  circle(40,0,50);
  fill(243,229,245);
  circle(-25,105,50);
  fill(243,229,245);
  circle(-20,-5,50);
  fill(243,229,245);
  circle(-55,50,50);
  pop();
}
}
