let angle = 0; //The angle of which the throbber starts
let rotationSpeed = 0.05; //Speed of the rotation

function setup(){
    createCanvas(windowWidth,windowHeight);
}

function draw(){
    background('ivory');
    fill(33,33,33);
    textFont('Courier New',35);
    text('Pause the Day...', 560,100);
    textFont('Courier New',20);
    text('Hold down the mouse, for a break', 530,140);

    push();
    translate(width/2, height/2); //Placing the throbber at the center
    rotate(angle);
    throbber();
    angle+=rotationSpeed; //Activating clockwise rotation, with speed. 
    pop();

    if (mouseIsPressed){ 
        rotationSpeed=0; //When the mouse is pressed down, the rotation will stop. 
    } else{
        rotationSpeed=0.05; //If the mouse is not pressed down, the rotation speed will return to normal 
    }
}

function throbber(){
    push();
    for (let i = 0; i < 5; i++){ //Making 1 line become 5 lines by the use of a for loop. 
        rotate(0.55); //Rotating the lines, so that they are only places on the 'sun'.
        stroke(255, 224, 130);
        line(-90,0,-55,1);}
    pop();
   
    //Sun
    push();
    noStroke();
    fill(255, 224, 130);
    arc(0, 0, 100, 100, 600, TWO_PI); 

    //Moon
    push();
    noStroke();
    fill(158, 158, 158);
    arc(0, 0, 100, 100, 0, PI);

    //Moon Spot
    fill(189, 189, 189);
    ellipse(25,28,25,20);

    fill(33,33,33,40);
    ellipse(25,28,20,15);

    fill(189, 189, 189);
    ellipse(-30,18,25,20);

    fill(33,33,33,40);
    ellipse(-30,18,20,15);
}

