**Title: Boxes, Boxes and more Boxes!** <br>

**Description:** <br>
This Minix shows generative program. The left side of the canvas shows a random, yet more structured placeing of boxes. Meanwhile the right side of the canvas shows a more unstructed placing of boxes. 

**Pictures:**
![Here are pictures of my miniX:](MiniX.5.1.png)
![Here are pictures of my miniX:](MiniX.5.2.png)
![Here are pictures of my miniX:](MiniX.5.3.png)

Here is a link to the RunMe: https://kaltrine1.gitlab.io/aestetiskprogrammering/Minix5/index.html

Here is a link to the Code: https://kaltrine1.gitlab.io/aestetiskprogrammering/Minix5/Minix5.js

**Reflections - What role do rules and processes have in your work?:** <br>

The focus on this miniX was to make a generative program, while incorporating two rules, a loop and a conditional statement. 
When thinking of an idea for the miniX, ideas of movement caught my interest. I wanted to create two different designs, that in the end would end up looking almost identical. Creating an even bigger 'art' piece. This is why i decided to split up the canvas, which is made clear by adding a black line() down the middle. The idea is for the left side to have boxes appearing at random width and height. This was made possible by creating an array further up, and later pushing out boxes at random places in my for loops. It is also by saying 'width/2' that the boxes only appear on the left side of the canvas, since the rule is for the boxes only to appear there. Another rule for the left canvas, was that the boxes had to appear again, when they reached the middle of the canvas. This has been done by adding a '-', in front of the 'width/2'. 
When adding the distanceBetween the boxes, they move in a line, on random x and y coordinates. 
 
The first rule goes again for the right half of the canvas. Here the rects that are appearing also have to start at a random place from middle to the end of the canvas. On the right half of the canvas, the rects are being scattered wherever they want, creating a more chaotic design. 
Another rule in this miniX, is for the rects to have a random color, on the left side of the canvas, all colors were included, but on the right side, the shades of red were excluded. This was so that the two designs were visible after having been filled out by boxes.
When including frameRate(10), the speed of which the rects appear on both sides become slower. 
<br>

**Reflections - Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?:** <br>
The miniX helps me understand the idea of 'auto-generator' better, since I got to do it on my own. The idea of something continuing to happen, without any interaction, but still having to imply certain rules, is fun to work with. And the idea of this miniX, was to try and showcase two different sides. One side that is more ’structured’, in terms of the design, while the other side is more chaotic. Both designs use the random function, however in the end, ends up looking almost identical, because they both keep on adding rects, the only thing that differes is the color. 
<br>


**References:** <br>
frameRate: https://p5js.org/reference/#/p5/frameRate <br>
Syntax for the for of loop - Lecture 10 - Aesthetic programming <br>
For: https://p5js.org/reference/#/p5/for <br>
Array: Lecture 10 - Aesthetic Programming <br>
Push: https://p5js.org/reference/#/p5/push <br>
Pop: https://p5js.org/reference/#/p5/pop <br>
NoStroke: https://p5js.org/reference/#/p5/noStroke <br>
Stroke: https://p5js.org/reference/#/p5/stroke <br>
StrokeWeight: https://p5js.org/reference/#/p5/strokeWeight <br>
Line: https://p5js.org/reference/#/p5/line <br>
If statement: https://p5js.org/reference/#/p5/if-else <br>
Random: https://p5js.org/reference/#/p5/random <br>
+= - Lecture 9 - Aesthetic programming <br>
Rect: https://p5js.org/reference/#/p5/rect <br>


