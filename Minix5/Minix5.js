let squares = [];
let boxes = [];
let distanceBetween = 15; // The distance between the boxes.

function setup(){
    createCanvas(windowWidth, 700);
    background('ivory');
    frameRate(10); //How fast the boxes appear on the canvas.

    for (let i = 0; i < 1; i++){ //How many squares start before it is filled with more, 1 box.
        squares.push({ //Pushing out several squares at random on the specified canvas.
            x: random(width/2,width),
            y: random(width/2,width),
        });
    }

    for (let i = 0; i < 5; i++){ //How many squares start before it is filled with more, 5 boxes.
        boxes.push({ // Pushing out several boxes at random on the specified canvas.
            x: random(width/2),
            y: random(width/2),
        });
    }
}

function draw(){

    //Drawing the line at the middle.
    push();
    stroke(0);
    strokeWeight(3);
    translate(width/2, height/2);
    line(0,-height/2,0,height);
    pop();

    //Calling the functions.
    drawSquares();
    drawBoxes();

}

function drawSquares(){

    for (let square of squares){ //For of loop. 

        if(square.y > 0 - 20){ //Making the boxes appear on the right side of the canvas at random height and width. 
            square.y = random(width/2);
            square.x = random(width/2, width);
        }

        fill(random(0), random(255), random(255)); //Filing the boxes with random color.
        noStroke();
        rect(square.x,square.y,10,10); //The box that can be placed on the right half, and the size. 
    }
}

function drawBoxes(){

    for (let box of boxes){ //For of loop. 

        if(box.x > width/2 - 10){ //If the boxes reach the line, then the boxes appear again, at random height and width, from the left side. 
            box.y = random(height);
            box.x = random(-width/2);
        }

        fill(random(255), random (255), random(255)); //Filling the boxes with random color. 
        noStroke();
        rect(box.x, box.y,10,10); //The box that can be placed on the left half, and the size. 
        box.x += distanceBetween; //The distance between the boxes. 

    }
}