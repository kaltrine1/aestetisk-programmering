class CookieJar {
  constructor() {
    // Set the position of the cookie jar at the center of the canvas
    this.pos = {
      x: width / 2,
      y: height / 2
    }
    // Set the size of the cookie jar
    this.size = {
      w: 450,
      h: 550
    }
    // Set the speed at which the cookie jar moves down
    this.speed = 5;
    // Set the target Y position for the cookie jar to move towards
    this.targetY = height;
  };

  move() {
    // Move the cookie jar towards the target Y position if it's not already there
    if (this.pos.y < this.targetY) {
      this.pos.y += this.speed;
    }
  };

  show() {
    // Display the image of the cookie jar at its current position with specified size
    image(cookieJarImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
  };
}
