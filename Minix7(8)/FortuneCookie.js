class FortuneCookie {
  constructor() {
      // Set the position of the fortune cookie at the center of the canvas
      this.pos = {
          x: width / 2,
          y: height / 2
      };
      // Set initial size of the fortune cookie
      this.size = {
          w: 100,
          h: 120
      };
      // Set the movement speed of the fortune cookie
      this.speed = 3;
      // Set target Y position for the fortune cookie to move upwards
      this.targetY = height / 2.5;
      // Set target size for the fortune cookie to expand towards
      this.targetSize = {
          w: 300,
          h: 360
      };
      // Define left x boundary for the left fortune cookie's movement
      this.targetLeftX = width * 0.35;
      // Define right x boundary for the right fortune cookie's movement
      this.targetRightX = width * 0.65;
  }

  // Method to move and expand the fortune cookie
  moveAndExpand() {
      // Move the fortune cookie towards the target Y position
      if (this.pos.y > this.targetY) {
          this.pos.y -= this.speed;
      }
      // Expand the size of the fortune cookie towards the target size
      if (this.size.w < this.targetSize.w || this.size.h < this.targetSize.h) {
          this.size.w += 2;
          this.size.h += 2;
      }
  }

  // Method to move the left fortune cookie to the left
  moveLeft() {
      // Move the left fortune cookie to the left within the defined left boundary
      if (this.pos.x > this.targetLeftX) {
          this.pos.x -= this.speed;
      }
  }

  // Method to display the left fortune cookie image when moving left
  showLeft() {
      // Display the left fortune cookie image
      image(leftFortuneCookieImg, this.pos.x+2, this.pos.y+3, this.size.w, this.size.h);
  }
}

// Class representing the right fortune cookie object, inheriting from FortuneCookie class
class FortuneCookieRight extends FortuneCookie {
  // Method to display the right fortune cookie image when moving right
  showRight() {
      // Display the right fortune cookie image
      image(rightFortuneCookieImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
  }

  // Method to move the right fortune cookie to the right
  moveRight() {
      // Move the right fortune cookie to the right within the defined right boundary
      if (this.pos.x < this.targetRightX) {
          this.pos.x += this.speed;
      }
  }
}