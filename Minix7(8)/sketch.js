// Declare global variables for images
let backgroundImg;
let rightFortuneCookieImg;
let leftFortuneCookieImg;
let cookieJarImg;
let noThanksCookieJarImg;
let noThanksButtonImg;
let music;

// Declare variable for the API
let advice;

// Declare variables for the objects cookie jar and fortune cookies
let cookieJar;
let fortuneCookie;
let fortuneCookieRight;

// Flags to track movement, button and jar visibility
let isJarAndCookieMoving = false;
let isLeftAndRightMoving = false;
let isNoThanksCookieJarShowing = false;
let isNoThanksButtonVisible = true;

// Preload music, images and JSON data
function preload() {
  music = loadSound('music.mp3');
  cookie = loadSound('cookie.mp3');
  backgroundImg = loadImage('background.jpg');
  rightFortuneCookieImg = loadImage('halfFortuneCookieRight.png');
  leftFortuneCookieImg = loadImage('halfFortuneCookieLeft.png');
  cookieJarImg = loadImage('fortuneCookieJar.png');
  noThanksCookieJarImg = loadImage('cookieJarNoThanks.png');
  noThanksButtonImg = loadImage('noThanksButton.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  imageMode(CENTER);
  music.loop();

  // Initialize objects
  fortuneCookie = new FortuneCookie();
  fortuneCookieRight = new FortuneCookieRight();
  cookieJar = new CookieJar();
}

function draw() {
  push();
  imageMode(CORNER);
  background(backgroundImg, width, height);
  pop();

  // Move objects when isJarAndCookieMoving is true. This moves the fortune cookie out of the jar and expands it
  if (isJarAndCookieMoving) {
    cookieJar.move();
    fortuneCookie.moveAndExpand();
    fortuneCookieRight.moveAndExpand();
  }

  // Move left and right fortune cookies when isLeftAndRightMoving is true. This moves the two cookie halves away from each other
  if (isLeftAndRightMoving) {
    fortuneCookieRight.moveRight();
    fortuneCookie.moveLeft();

    // Variables to define paper coordinates for fortune message, based on the coordinates from the fortune cookie 
    let paperX1 = fortuneCookie.pos.x;
    let paperY1 = fortuneCookie.pos.y - 40;
    let paperX2 = fortuneCookieRight.pos.x + 50;
    let paperY2 = fortuneCookieRight.pos.y - 40;
    let paperX3 = fortuneCookieRight.pos.x + 50;
    let paperY3 = fortuneCookieRight.pos.y - 140;
    let paperX4 = fortuneCookie.pos.x;
    let paperY4 = fortuneCookie.pos.y - 140;

    // Draw paper for fortune message
    drawPaper(paperX1, paperY1,
      paperX2, paperY2,
      paperX3, paperY3,
      paperX4, paperY4);

    // Display advice text on paper
    showText();
  }

  // Show the main elements
  showMainElements();

  // Show "No Thanks" cookie jar when isNoThanksCookieJarShowing is true
  if (isNoThanksCookieJarShowing) {
    noThanksCookieJar();
  }
}

// Function to display main elements
function showMainElements() {
  fortuneCookieRight.showRight();
  fortuneCookie.showLeft();
  cookieJar.show();
  noThanksButton();
}

// Function to display "No Thanks" button
function noThanksButton() {
  // Display "No Thanks" button when isNoThanksButtonVisible is true
  if (isNoThanksButtonVisible) {
    image(noThanksButtonImg, width / 2, height * 0.88, 192, 58);
  }
}

// Function to display "No Thanks" cookie jar screen
function noThanksCookieJar() {
  // Display "No Thanks" cookie jar image and stop draw loop
  image(noThanksCookieJarImg, width / 2, height / 2, 450, 550);
  noLoop();
}

// Function to get advice from API
function getAdvice() {
  loadJSON('https://api.adviceslip.com/advice', gotAdvice);
}

// Callback function for advice API
function gotAdvice(data) {
  advice = data.slip.advice; // Store advice data
}

// Function to draw paper for fortune messages
function drawPaper(x1, y1, x2, y2, x3, y3, x4, y4) {
  push();
  fill(255);
  noStroke();
  rectMode(CENTER);
  quad(x1, y1,
    x2, y2,
    x3, y3,
    x4, y4);
  pop();
}

// Function to display advice text
function showText() {
  textAlign(CENTER, CENTER);
  textSize(20);
  fill(0);
  text(advice, width / 2.6, fortuneCookie.pos.y - 240, 400, 300);
}

// Handle mouse clicks
function mouseClicked() {
  if (   // Checks if cookieJar has been clikced
    mouseX > cookieJar.pos.x - cookieJar.size.w / 2 &&
    mouseX < cookieJar.pos.x + cookieJar.size.w / 2 &&
    mouseY > cookieJar.pos.y - cookieJar.size.h / 2 &&
    mouseY < cookieJar.pos.y + cookieJar.size.h / 2
  ) {
    isJarAndCookieMoving = true; // Start moving cookie jar and fortune cookie
    isNoThanksButtonVisible = false; // Hide "No Thanks" button
  } else if ( // Checks if fortuneCookie has been clicked
    mouseX > fortuneCookie.pos.x - fortuneCookie.size.w / 2 &&
    mouseX < fortuneCookie.pos.x + fortuneCookie.size.w / 2 &&
    mouseY > fortuneCookie.pos.y - fortuneCookie.size.h / 2 &&
    mouseY < fortuneCookie.pos.y + fortuneCookie.size.h / 2
  ) {
    isLeftAndRightMoving = true; // Start moving left and right fortune cookies away from each other
    isNoThanksButtonVisible = false; // Hide "No Thanks" button
    getAdvice(); // Get advice from API
    cookie.play();
  } else if ( // Checks if noThanksButton has been clicked
    mouseX > width / 2 - 192 / 2 &&
    mouseX < width / 2 + 192 / 2 &&
    mouseY > height * 0.88 - 58 / 2 &&
    mouseY < height * 0.88 + 58 / 2
  ) {
    isNoThanksCookieJarShowing = true; // Show "No Thanks" cookie jar screen
    isNoThanksButtonVisible = false; // Hide "No Thanks" button
  }
}