**Title:** Goodadvice.com <br>

**Description:** <br>
This project is an interactive fortune cookie program which gives random “personalized” advice when the cookie jar has been pressed.  
When you start the program, you choose whether or not you want advice. If you are up for it, you get to “break” open a fortune cookie, and to reveal some words of wisdom! This all works thanks to an API of advice - some good, some questionable, but advice, nonetheless.  
If the user doesn’t want advice, it is possible to press the ‘Not today, but thanks!’ button, which then stops the program, with a noLoop(). <br> 

![Picture of the program](still_frame_api.png)

Here is a link to the runMe: https://kaltrine1.gitlab.io/aestetiskprogrammering/Minix7(8)/index.html 

Here is a link to the source code: https://kaltrine1.gitlab.io/aestetiskprogrammering/Minix7(8)/sketch.js 

**How it works:** <br>

Run the program: 
Choose whether to receive advice. 
Click to break open the fortune cookie. 
Receive and read the advice provided. <br> 

**API integration:** <br>

The project integrates with the Advice Slip API to provide random advice to the user. The API returns a JSON object containing a piece of advice, which is then displayed to the user when they choose to receive advice. <br>

**Our experience with JSON API:** <br>

We decided to go with the Advice Slip API, because of the easy accessibility of the API, but also because of the documentation provided, it is simple and straight forward. The ideas for the MiniX were based on the API, and how it could be implemented. With this, the idea of a fortune cookie came to mind. We decided to go with the idea, even though the API does not provide fortunes, but general advice instead, since it still fits the overall idea and concept of the program we wanted to create. <br>
 
With this API we do not really change the data but show the API as the original text form without manipulating how it is displayed. We have some degree of control over how we utilize the API within our own programs or applications, but it is the provider, who has control over the API, including its functionality, availability, and terms of use. They dictate how users can interact with the API and may enforce limits or restrictions on usage. <br>

APIs play a pivotal role in digital culture by granting programmers and users access to data and functionalities that may otherwise be inaccessible. They empower developers to create applications that draw upon a wide spectrum of resources, such as weather forecasts or other platform data. Additionally, APIs can be seen as a way of outsourcing work to other platforms or services. By using APIs, developers can integrate existing functionalities into their own applications without needing to build everything from scratch. Therefore, APIs serve as a means of streamlining development efforts. This not only enhances innovation and collaboration but also fosters efficiency and rapid digital development. <br>
 
**If given more time:** <br>

If we had more time, we would have fixed the place you need to click to get another advice.  <br>

Something we would like to refine when looking at the program, would be the place where the user is able to click the fortune cookie, to get another advice. If possible we would have preferred that when the user presses to get another advice, that the fortune cookie would become whole again, for it to be broken, and seem like a new fortune. <br> 

Given more time we could perhaps have looked further into the amount of apps, programs or websites that we make use of in our everyday life, that also contain functions of API, or at least has an API usage to make the programs run as intended. Especially because we think that it could be way more than we realize. <br>
 
Reflecting upon API’s we find it to be a very useful resource for getting massive amounts of data, that is already there and available. We then discussed how we used the advice slip API, and how we pretty much used it as it is. Whereas it would have been interesting to make use of the API in a different way by manipulating the way the data is visualised. Another idea would have been to incorporate two different types of API’s and make them somehow interact, to create a more complex program. <br>
 
These are some different ways we would have refined our program if given more time, and also some thoughts we had during our ideation process. <br>
 
**References:** <br> 
image() (https://p5js.org/reference/#/p5/image) <br>
if-else (https://p5js.org/reference/#/p5/if-else) <br>
class (https://p5js.org/reference/#/p5/class) <br>
imageMode() (https://p5js.org/reference/#/p5/imageMode) <br>
text() (https://p5js.org/reference/#/p5/text) <br>
textAllign() (https://p5js.org/reference/#/p5/textAlign) <br>
intheritance (http://staging.p5js.org/examples/objects-inheritance.html) <br>
loadJSON (https://p5js.org/reference/#/p5/loadJSON) <br>
loadSound() (https://p5js.org/reference/#/p5/loadSound) <br>
preload() (https://p5js.org/reference/#/p5/preload) <br>
noLoop() (https://p5js.org/reference/#/p5/noLoop) <br>
API (https://api.adviceslip.com/advice) <br>