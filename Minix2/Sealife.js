function preload() {
  img1 = loadImage('flaske.png');
  img2 = loadImage('straws.png');
  img3 = loadImage('Fishingnet.png');
  } 
  
function setup(){
  createCanvas(windowWidth,windowHeight);
  background(255, 254, 242);
  imageMode(CORNER);
  textFont('Courier New',35);
  text('Press Anywhere to Show the Truth',400,100);
  }
  
function mousePressed(){
  Ocean(mouseX-550, mouseY-300); //So that the start of the emojis are where the mouse is pressed. 
  }
  
function Ocean(x,y){
  push();
  translate(x,y);
  //Turtle Emoji 
  noStroke();
  fill(187, 222, 251);
  rect(550,300,300,300,20);
  image(img1, 580, 350, 95, 95);
  image(img2, 730, 335, 90, 90);
  image(img3,552,308,290,290);
  fill(33, 33, 33);
  textFont('Courier New',25);
  text('Turtle',650,330);
  
  //Body
  fill(56, 142, 60);
  ellipse(700,505,150,100);
  
  //Small Spot 1
  fill(104, 159, 56);
  ellipse(675,485,40,20);
  
  //Small Spot 2
  fill(158, 157, 36);
  ellipse(725,495,35,20);
  
  //Small Spot 3
  fill(158, 157, 36);
  ellipse(677,520,35,20);
  
  //Small Spot 4
  fill(104, 159, 56);
  ellipse(725,525,35,20);
  
  //Head
  fill(124, 179, 66);
  circle(600,490,60);
  fill(33, 33, 33);
  circle(580,490,5);
  
  //Feet
  fill(124, 179, 66);
  circle(680,565,25);
  circle(730,563,25);
  
  //Coral Reef Emoji
  noStroke();
  fill(187, 222, 251);
  rect(900,300,300,300,20);
  
  //Border
  stroke(33, 33, 33);
  line(920, 350, 920, 560);
  stroke(33, 33, 33);
  line(1180, 350, 1180, 560);
  stroke(33, 33, 33);
  line(920, 350, 1180, 350);
  stroke(33, 33, 33);
  line(920, 560, 1180, 560);
  
  //Sea Bottom
  noStroke();
  fill(229, 195, 162)
  rect(920,540,260,20);
  
  //Coral Reef Shapes
  fill(33, 33, 33);
  textFont('Courier New',25);
  text('Coral Reef',970,330);
  fill(128, 120, 120)
  bezier(1050, 545, 1050, 450, 1000, 450, 1050, 545);
  fill(186, 162, 137)
  bezier(1070, 545, 1070, 450, 1120, 450, 1070, 545);
  fill(123, 108, 93)
  bezier(1060, 545, 1030, 400, 1080, 400, 1060, 545);
  fill(203, 163, 90)
  bezier(1075, 545, 1130, 400, 1170, 450, 1075, 545);
  fill(203, 163, 90)
  bezier(1040, 545, 950, 410, 1005, 410, 1040, 545);
  fill(128, 120, 120)
  bezier(1030, 545, 920, 460, 1000, 460, 1030, 545);
  fill(131, 104, 113)
  bezier(1055, 545, 1050, 510, 1115, 500, 1055, 545);
  fill(110, 90, 109)
  bezier(1050, 545, 995, 510, 1050, 500, 1050, 545);
    
  //Seaweed
  fill(195, 178, 126);
  ellipse(947,540,35,15);
  fill(106, 127, 84);
  ellipse(947,500,5,70);
  fill(106, 127, 84);
  circle(938,513,13);
  fill(106, 127, 84);
  circle(938,498,13);
  fill(106, 127, 84);
  circle(938,483,13);
  fill(106, 127, 84);
  circle(938,468,13);
  fill(106, 127, 84);
  circle(956,483,13);
  fill(106, 127, 84);
  circle(956,498,13);
  fill(106, 127, 84);
  circle(956,513,13);
  fill(106, 127, 84);
  circle(956,468,13);
  
  //Grass
  fill(106, 127, 84);
  ellipse(926,540,5,25);
  fill(126, 131, 104);
  ellipse(933,545,5,25);
  fill(126, 131, 104);
  ellipse(952,543,5,25);
  fill(106, 127, 84)
  ellipse(960,540,5,25);
  fill(106, 127, 84);
  ellipse(970,543,5,25);
  fill(126, 131, 104);
  ellipse(990,540,5,25);
  fill(106, 127, 84);
  ellipse(1000,543,5,25);
  fill(126, 131, 104);
  ellipse(1105,543,5,25);
  fill(106, 127, 84);
  ellipse(1113,540,5,25);
  fill(126, 131, 104);
  ellipse(1121,542,5,25);
  fill(106, 127, 84);
  ellipse(1146,542,5,25);
  fill(126, 131, 104);
  ellipse(1155,540,5,25);
    
  //Coral Reef Shape Pink 
  fill(171, 120, 154);
  circle(1020,550,12);
  fill(171, 120, 154);
  circle(1030,546,12);
  fill(171, 120, 154);
  circle(1030,553,12);
  fill(171, 120, 154);
  circle(1040,545,12);
  fill(171, 120, 154);
  circle(1040,552,12);
  fill(171, 120, 154);
  circle(1050,548,12);
  pop();
  }
  