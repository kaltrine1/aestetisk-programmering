let fish;
let scl = 70;
let speed = 15;
let stones = [];
let min_stone = 5; //The minimum amout of stones shown on the canvas. 

class Fish { //Make an object that has an x and y, and also a speed. The size of the fish is also adjusted here. 
    constructor() {
        this.x = 0;
        this.y = 0;
        this.xspeed = 1;
        this.yspeed = 0;
        this.size = 80;
    }

    dir(x, y) { //Set the objects direction 
        this.xspeed = x;
        this.yspeed = y;
    }

    update() {
        this.x = this.x + this.xspeed * speed;
        this.y = this.y + this.yspeed * speed;

        this.x = constrain(this.x, 0, width - scl);
        this.y = constrain(this.y, 0, height - scl);
    }

    show() {
        image(fishy, this.x, this.y, this.size, this.size);
    }
}

class Stone { // Create the object stone, and make it appear at a random x and y coordinate. 
    constructor() {
        this.x = random(0, width);
        this.y = random(0, height);
        this.speed = random(10, 20); //Let it be a random speed.
        this.size = 50 // The size of the stones
    }

    move() { //Make the stones move downwards from the top
        this.y += this.speed;

        if (this.y > height) { //Make the stones appear again after going of the screen. 
            this.y = random(0, -height);
            this.x = random(0, width);
        }
    }

    show() {
        image(stoney, this.x, this.y, this.size, this.size);
    }
}

function preload() { //Load the gifs and pictures that are used in the game
    ocean = loadImage("ocean.jpg");
    fishy = loadImage("Dory.gif");
    stoney = loadImage("stone.gif");
}

function setup() {
    createCanvas(700, 600);
    fish = new Fish();
    frameRate(10);
}

function draw() { //Call the functions and show the fish
    background(ocean);
    fish.update();
    fish.show();
    amountOfStones();
    showStone();

    for (let stone of stones) {//Calculating the distance between the fish and the stone
        if (dist(fish.x + fish.size / 2, fish.y + fish.size / 2, stone.x + 25, stone.y + 25) < (fish.size / 2) + (stone.size / 2)) {
            gameOver();
        }
    }
}

function gameOver() {//Making the 'game over' text 
    textFont("Courier New", 40)
    fill(211, 47, 47);
    stroke(1);
    strokeWeight(10);
    textAlign(CENTER);
    text("Game Over", 350, 300);
    
    textFont("Courier New", 20)
    fill(211, 47, 47);
    stroke(1);
    strokeWeight(10);
    text("Refresh to start again", 350, 330);
    noLoop();
}

function amountOfStones() { //Push out new stones with the same proporties 
    if (stones.length < min_stone) {
        stones.push(new Stone());
    }
}

function showStone() { // Show the stones that appear 
    for (let stone of stones) {
        stone.move();
        stone.show();
    }
}

function keyPressed() { //How to operate the game, and which direction the fish can go. 
    if (keyCode === UP_ARROW) {
        fish.dir(0, -1);
    } else if (keyCode === DOWN_ARROW) {
        fish.dir(0, 1);
    } else if (keyCode === LEFT_ARROW) {
        fish.dir(-1, 0);
    } else if (keyCode === RIGHT_ARROW) {
        fish.dir(1, 0);
    }
}